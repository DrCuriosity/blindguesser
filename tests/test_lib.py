import unittest
import blindguesser


class TestLibrary(unittest.TestCase):
    def setUp(self):
        self.guesser = blindguesser.BlindGuesser()

    def test_get_middle_point(self):
        # Edge cases
        self.assertEqual(self.guesser.get_middle_point(""), False,
                         'sequence is empty string')
        self.assertEqual(self.guesser.get_middle_point([]), False,
                         'sequence is empty list')
        self.assertEqual(self.guesser.get_middle_point("a"), False,
                         'sequence is single character in string')
        self.assertEqual(self.guesser.get_middle_point(["a"]), False,
                         'sequence is single character in list')
        self.assertEqual(self.guesser.get_middle_point(b'a'), False,
                         'sequence is single character in byte')

        # Main logic
        self.assertEqual(self.guesser.get_middle_point("ab"), 1,
                         'even length rounds up (2)')
        self.assertEqual(self.guesser.get_middle_point("abcdef"), 3,
                         'even length rounds up (6)')
        self.assertEqual(self.guesser.get_middle_point("abc"), 1,
                         'odd length finds centre (3)')
        self.assertEqual(self.guesser.get_middle_point("abcdefghi"), 4,
                         'odd length finds centre (9)')

        # As applied to subsequences
        nine = "abcdefghi"
        pivot_1 = self.guesser.get_middle_point(nine)
        self.assertEqual(nine[pivot_1:], "efghi",
                         'second subquence after one split')
        pivot_2 = self.guesser.get_middle_point(nine[pivot_1:], min_index=pivot_1)
        self.assertEqual(nine[pivot_1:pivot_2], "ef",
                         'third subquence after two splits')
