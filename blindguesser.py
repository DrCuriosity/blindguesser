from abc import abstractmethod
from math import log

import requests


class BlindGuesser:
    """Base class for binary search blind command injection probes."""

    NUMBERS = "0123456789"
    UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
    FULL_VOCAB_ALPHANUM_REGEX = NUMBERS + UPPERCASE + LOWERCASE

    def build_filtered_vocabulary(self, full_vocab, http_auth=False,
                                  debug=False):
        """Build and return a filtered vocabulary for the target text.
        - full_vocab : every potential symbol to test, in a string
        - http_auth : credentials if required
        - debug : print intermediate results as we go?

        Raises: an Exception if it gets a response that's not a 200 - OK.
        Returns: a filtered set of symbols found.
        """
        if debug:
            print("Reduce vocabulary from: {} ({} chars)".format(full_vocab, len(full_vocab)))
        filtered_vocab = ""
        for symbol in full_vocab:
            url = self.build_url_function(symbol)
            # print(url)
            if http_auth:
                resp = requests.get(url, auth=http_auth)
            else:
                resp = requests.get(url)

            if resp.status_code == 200:
                if self.output_found_filter(resp):
                    filtered_vocab += symbol
                    if debug:
                        print(filtered_vocab)
            else:
                raise Exception("Unexpected HTTP response code: {}".format(resp.status_code))

        if debug:
            print("Reduced to {} ({} chars)".format(filtered_vocab, len(filtered_vocab)))
            total = len(full_vocab)
            filtered = len(filtered_vocab)
            removed = total - filtered
            print("* Reduction in search space: {0:.0%} of symbols".format(removed / total))
            print("* Estimated reduction in guesses per character: {:.2}".format(
                log(total, 2) - log(filtered, 2))
            )
        return filtered_vocab

    def regex_binary_search(self, vocab, sofar='', http_auth=False, debug=False):
        """Determine a target symbol via binary search.
        - vocab : A list of all possible symbols
        - sofar : A string of symbols unconvered so far
        - http_auth : A (http_user, http_pass) tuple, if used.
        - debug : print intemediate results as we go?
        """
        min_s, max_s = 0, len(vocab)
        pivot_count = 0

        while True:
            pivot = self.get_middle_point(vocab[min_s:max_s], min_index=min_s)
            if pivot is False:
                break
            if pivot_count >= self.MAX_PIVOT:
                break
            pivot_count += 1

            search = '^' + sofar + '[{}-{}]'.format(
                vocab[min_s],
                vocab[pivot - 1]
            )
            url = self.build_url_function(search)

            if debug:
                print('')
                print("* Pivot Count: " + str(pivot_count))
                print("* Search: '{}', pivoting on {}".format(
                    vocab[min_s:max_s],
                    vocab[pivot]
                ))
                print("* Guessing in '{}' not '{}' (idx [{}:{}] or [{}:{}]):".format(
                    vocab[min_s:pivot],
                    vocab[pivot:max_s],
                    min_s,
                    pivot,
                    pivot,
                    max_s
                ))
                print("* Regex: {}".format(search))

                print("* URL: {}".format(url))

            if self.output_found_filter(requests.get(url, auth=http_auth)):
                max_s = pivot
                if debug:
                    print("<<< Target on LHS <<<")
            else:
                min_s = pivot
                if debug:
                    print(">>> Target on RHS >>>")

        newfound = vocab[min_s]
        if debug:
            print("Symbol found: {}".format(newfound))
        return(sofar + newfound)

    @abstractmethod
    def output_found_filter(self, response):
        """
        Issue a request that determines whether the symbol input is valid (found) or not.

        Returns: a boolean result
        """
        pass

    @abstractmethod
    def build_url_function(self, part):
        """
        Build a request URL for probing the target that a "part" guess can be injected into.

        - part : the part of the URL that contains (a sequence or pattern) to
          test for via blind command insertion.
        """
        pass

    def get_middle_point(self, seq, min_index=0):
        """Find the middle point of a sequence of symbols.

        This sequence may be a subseqence within a larger sequence of symbols.
        Given an even number of symbols, the middle point will be rounded up.

        For example, given the string:

        "abcdef"

        The middle point is partway between 'c' and 'd', and this function will return 3.

        As Python indices start at 0, "abcdef"[3] is 'd'.


        Arguments:
        - seq : a sequence of symbols
        - min_index : (optional) the minimum index of of a subsequence, to be added to the result.

        Returns:
        - The integer index of the middle point of sequence
        - False if the sequence has no middle point (0 or 1 items)
        """
        seq_length = len(seq)
        if seq_length == 0:
            return False
        elif seq_length == 1:
            return False
        else:
            return min_index + int(len(seq) / 2)
