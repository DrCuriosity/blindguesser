# BlindGuesser

BlindGuesser is a Python library for performing binary search
(divide-and-conquer) probes for use in blind command insertion.

For more information, see:
* https://owasp.org/www-community/attacks/Command_Injection
* https://owasp.org/www-community/attacks/Blind_SQL_Injection


A binary search approach makes use of two features within an insecure web
appliance:

* the ability to craft a request that determines a yes/no response on whether a
  search is successful

* the ability to search on ranges of symbols that could be in the output, as
  opposed to individual characters

The exact mechanisms for range specification and result identification may vary,
so the library is implemented as an abstract base class that can be subclassed
and modified to suit. Examples are provided in the `examples` directory.

This library is provided for proof-of-concept and automated penetration testing
purposes. (Don't do crimes.)


## Requirements

Python 3, with the [`requests`](https://requests.readthedocs.io/) library.
The examples also use [`vcrpy`](https://vcrpy.readthedocs.io/) for
recording and replay of requests, so that you can see the requests being made.

You may wish to use a [virtual environment](https://docs.python.org/3/library/venv.html)
for installing requirements:

```sh
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
# To run examples:
pip install -r example/requirements.txt
```

## Running examples

Add the HTTP password from Natas level 15&rarr;16 to your `examples/natas_16.ini` file,
then:

```sh
cd examples
python natas_16.py
```
